import React from 'react';

import './Totalprice.css';

const Totalprice = props => (
    <p className="totalPrice">Total price is: {props.priceOfItems} KGS</p>
);

export default Totalprice;