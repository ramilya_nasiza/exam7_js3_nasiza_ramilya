import React from 'react';

import './Order.css';

const Order = props => (
  <div className='order'>
    <span className='orderTitle'>{props.title}</span>
    <span>{'x ' + props.count}</span>
    <span>{props.price + ' KGS'}</span>
    <button className='remove' onClick={props.remove}>X</button>
  </div>
);

export default Order;