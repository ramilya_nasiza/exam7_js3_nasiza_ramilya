import React from 'react';

import './Item.css';

const Item = props => (
  <div className='itemBox'>
    <button className='item' onClick={props.addItem}>
      <img src={props.image} alt="" className='img'/>
      <div className='text'>
        <p>{props.title}</p>
        <p className='itemPrice'>{'Price: ' + props.price + ' KGS'}</p>
      </div>
    </button>
  </div>
);

export default Item;