import React, { Component } from 'react';
import ChewingGum from '../../assets/bubblegym.jpg';
import Cola from '../../assets/cola.png';
import Esse from '../../assets/esse.png';
import Heineken from '../../assets/heineken.png';
import Twix from '../../assets/twix.png';
import Parlament from '../../assets/parlament.jpeg';


import './App.css';
import Item from "../../components/Item/Item";
import Order from "../../components/Order/Order";
import Totalprice from "../../components/Totalprice/Totalprice";

class App extends Component {

  state = {
    items: [
      {name: 'Chewing Gum', count: 0, price: 30, image: ChewingGum},
      {name: 'Coca Cola', count: 0, price: 35, image: Cola},
      {name: 'Esse', count: 0, price: 80, image: Esse},
      {name: 'Heineken beer', count: 0, price: 110, image: Heineken},
      {name: 'Twix', count: 0, price: 35, image: Twix},
      {name: 'Parlament Silver', count: 0, price: 105, image: Parlament}
    ]
  };

  addItemForOrder (event, index) {
    event.preventDefault();
    let items = [...this.state.items];
    let item = {...items[index]};
    item.count++;
    items[index] = item;

    this.setState({items});
  };

  getTotal = () => {
    let total = 0;
    this.state.items.map((order) => {
      total += (order.count * order.price);
    });
    return total;
  };

  removeItem (event, index) {
    event.preventDefault();
    let items = [...this.state.items];
    let item = {...items[index]};
    item.count = 0;
    items[index] = item;

    this.setState({items});

  };

  render() {

    let sum = this.getTotal();

    return (
      <div className="App">
        <div className='itemsDiv'>
          Items:
          <div className='menu'>
            {this.state.items.map((item, index) => {
              return <Item
                  image={item.image}
                  title={item.name}
                  price={item.price}
                  key={index}
                  addItem={event => this.addItemForOrder(event, index)}
              />
            })}
          </div>
        </div>
        <div className='orderDiv'>
          Order Details:
            {(sum !== 0) ? this.state.items.map((order, index) => {
              if(order.count !== 0){
                return <Order
                    title={order.name}
                    count={order.count}
                    price={order.price}
                    key={index}
                    remove={event => this.removeItem(event, index)}
                />
              }
            }): <p>Order is Empty!</p>}
          <Totalprice priceOfItems={sum}/>
        </div>
      </div>
    );
  }
}

export default App;
